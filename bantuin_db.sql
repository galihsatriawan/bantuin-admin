/*
Navicat MySQL Data Transfer

Source Server         : benerin
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : benerin_db

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2018-10-04 22:12:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `no_telp` varchar(12) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`no_telp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('085706749886', 'dirtaputra');
INSERT INTO `admin` VALUES ('admin', 'admin');

-- ----------------------------
-- Table structure for tb_customer
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer`;
CREATE TABLE `tb_customer` (
  `telp_customer` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `nama_customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_customer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_daftar` datetime NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `foto_customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token_customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `point` int(11) NOT NULL,
  PRIMARY KEY (`telp_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_customer
-- ----------------------------
INSERT INTO `tb_customer` VALUES ('+62', 'ika febrianti s', null, 'gitawisata87@gmail.com', '2018-07-19 12:20:08', '34', '1996-11-25', '', 'OKe', '0');
INSERT INTO `tb_customer` VALUES ('+620821252255121', 'Flori ', null, 'fitriflorianti@gmail.com ', '2018-08-01 18:38:39', '74', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+62082131305081', 'Fadly Usman', null, 'fadlypwkftub@gmail.com', '2018-07-26 08:07:23', '65', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628113230022', 'Renaldy', null, 'hivonon@gmail.com', '2018-07-18 20:56:27', '20', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628113666166', 'traju31', ' perumahan villa bukit sengkaling blok ae 07 malang', 'trajupamungkas31@gmail.com', '2018-07-26 15:53:39', '69', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628115402047', 'ade', null, 'ademalyadi@yahoo.co.id', '2018-07-21 09:32:10', '54', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628121697600', 'ibrahim', null, 'eaglestreet07@gmail.com', '2018-07-24 09:34:34', '62', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281230066064', 'yosrizal', null, 'atomyyos@gmail.com', '2018-08-01 09:46:47', '72', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281231066555', 'Amin Subagyo', null, 'amin.arema_dk@yahoo.co.id', '2018-07-25 10:19:51', '64', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281233448041', 'Yunna', null, 'yunnaahmad@gmail.com', '2018-07-30 13:38:03', '71', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281249688824', 'Triyuni Pratiwi', '', 'triyunipratiwi87@gmail.com', '2018-07-19 07:40:04', '26', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281316869630', 'Maing', ' ', 'mboiz.1578@gmail.com', '2018-07-19 11:16:01', '32', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281320101024', 'Bayu', null, 'mbayuindratomo@gmail.com', '2018-07-20 10:06:49', '49', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281321001024', 'Bayu', null, 'mbayuindratomo@gmail.com', '2018-07-20 10:04:44', '48', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628132449540', 'ALMA MAXFIRA', null, 'almaxfira542@gmail.com', '2018-07-19 16:23:43', '40', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281333119231', 'dian', null, '', '2018-07-19 20:35:16', '45', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281334734410', 'asfi', null, 'asfifsa@gmail.com', '2018-07-25 09:01:20', '63', '1996-11-25', '', 'dHoEpA3tuCA:APA91bHC0B9c3y7orELqRWh8QiiH4AalEiFJ7TrIYczANinGmJXM3hR6fLdehsPVoLA-mFMcJo09oE2NOi9wDGe9Du2EKKZIO9llj4EoeBjrO7Wir9KsUag_RR8OjkP__oH1zO39TAN6zQVPGqnljVLfA48nlnWmBw', '20');
INSERT INTO `tb_customer` VALUES ('+6281334939303', 'Sadewa', null, 'achmadbagussadewa@gmail.com', '2018-07-19 09:02:52', '27', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628135709444', 'Vanda', null, 'rosvanda.iti@gmail.com', '2018-07-21 16:03:00', '57', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281357094448', 'Vanda', null, 'rosvanda.iti@gmail.com', '2018-07-21 16:05:25', '58', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281513551898', 'anto', null, 'syamarifianto71@gmail.com', '2018-07-21 20:20:27', '60', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281556600770', 'yp', null, 'yosiapra@gmail.com', '2018-07-26 10:16:38', '68', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+62816934669', 'Fahmi Zakaria', null, 'fahmizakaria25@gmail.com', '2018-07-19 10:15:36', '31', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+62818811771', 'Utiek Narotomo', null, 'utiekputri@gmail.com', '2018-07-23 09:44:46', '61', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281905689342', 'Basuki', null, 'bisnisberjaya@gmail.com', '2018-07-20 10:31:51', '52', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281905689345', 'Basuki', null, 'bisnisberjaya@gmail.com', '2018-07-20 10:30:50', '51', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281937093405', 'Ricky WP', null, 'rickywicaksonop@gmail.com', '2018-07-19 13:30:57', '36', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281937767788', 'Antok', null, 'novianto@contractor.net', '2018-08-01 12:02:46', '73', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6281939179079', 'chyntia', null, 'chyntiad32@gmail.com', '2018-07-18 14:43:28', '15', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282132449540', 'ALMA MAXFIRA', null, 'almaxfira542@gmail.com', '2018-07-19 16:24:58', '41', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282143587663', 'Aisyah', null, 'pratitatita35@gmail.com', '2018-07-19 20:03:17', '44', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282220029272', 'dewasetyadji', null, 'dewasetyadji@gmail.com', '2018-08-02 02:49:24', '76', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282233605220', 'Fahmi Zakaria', null, 'fahmizakaria25@gmail.com', '2018-07-19 10:13:10', '30', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282233816676', 'sam harry', null, '53tyo.utomo78@gmail.com', '2018-07-21 16:38:36', '59', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282257875066', 'ika febrianti s', ' ', 'ika.febrianti.s88@gmail.com', '2018-07-19 12:21:49', '35', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6282338898020', 'satrio', null, 'nizaar.fs24@gmail.com', '2018-07-18 21:22:32', '21', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628312751091', 'Ilham Juney Rahman', null, 'rahmanjuney@gmail.com', '2018-07-17 21:48:23', '10', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6283129751091', 'Ilham Juney Rahman', null, 'rahmanjuney@gmail.com', '2018-07-17 21:49:08', '11', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6283834906049', 'han', null, 'enz0@bugcrowdninja.com', '2018-07-19 07:28:57', '24', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6283848555313', 'fahira', null, 'vera756@live.com', '2018-07-20 18:15:15', '53', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285235142348', 'uun', null, '', '2018-07-18 14:45:52', '16', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285246359510', 'Yasir', null, 'inijamur@gmail.com', '2018-07-19 00:19:41', '23', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285293943686', 'Reska Wijayanto', null, 'reskaundip_08@yahoo.co.id', '2018-07-20 07:29:02', '47', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285330330373', 'michael', null, 'michael280900@gmail.com', '2018-07-18 22:39:36', '22', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628556300955', 'defi', null, 'defidifi1@gmail.com', '2018-07-18 20:11:27', '18', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285604044550', 'Galih', 'Malang', 'Galih', '2018-07-17 21:01:42', '4', '1996-11-25', '', '', '36');
INSERT INTO `tb_customer` VALUES ('+6285608199608', 'Eddy Gutomo', null, 'eddyg5506@gmail.com', '2018-07-26 09:41:53', '67', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285706749886', 'dirta putra anggara', 'jalan raya bangilan pandanajeng', 'dirtaputraanggara@gmail.com', '2018-07-17 21:21:55', '5', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285730083702', 'febri', null, 'febri.yohanes46@gmail.com', '2018-07-17 21:28:12', '7', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285730702661', 'rhesal', 'Jalan Raya', 'rhesal.mahadyanto12@gmail.com', '2018-07-17 19:44:15', '1', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285745742457', 'Ganang ', null, 'cienlong50@gmail.com ', '2018-07-18 19:33:03', '17', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285749518864', 'yuliakartika', null, 'yuliakartika173@gmail.com', '2018-07-19 09:55:16', '29', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285790970998', 'Helmy A', null, 'helmyandria@gmail.com', '2018-07-18 12:55:27', '14', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285791104447', 'Nikki Rufiansya', null, 'nikkirufiansya88@gmail.com', '2018-07-17 21:41:26', '8', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285809060691', 'Athafy_Fahmi', 'Jl. H.M Ismail Rt14 Rw02 Des. Jatikerto, Kec. Kromengan, Kab. Malang', 'dahtafy@gmail.com', '2018-07-19 17:31:16', '42', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285855456777', 'suryandaru', null, 'suryandarususilo@gmail.com', '2018-07-20 10:29:05', '50', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285856767017', 'galang arbi', null, 'galangarbis@gmail.com', '2018-08-01 19:07:11', '75', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285857616503', 'ari', null, 'gung.d.himura@gmail.com', '2018-07-19 16:06:17', '39', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6285965543381', 'defi', null, 'defidifi1@gmail.com', '2018-07-18 20:27:03', '19', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6287765511113', 'Yatna Asintiyo', null, 'tyomboiz@gmail.com', '2018-07-19 19:27:50', '43', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6287825734258', 'Niko', null, 'nikop0003@gmail.com', '2018-07-29 08:47:28', '70', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6287851865395', 'Anando Sang Binatoro', null, 'leonandoz1152@gmail.com', '2018-07-18 11:19:37', '13', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6287877429556', 'dian', null, '', '2018-07-19 20:36:28', '46', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628813314581', 'Johan', null, 'jfirmanu@gmail.com', '2018-07-19 11:27:17', '33', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+628815034782', 'Duiddo Imaani Mohammad', null, 'duiddom@gmail.com', '2018-07-21 11:18:42', '55', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6288220036390', 'linda', null, 'rahayulindadwi7@gmail.com', '2018-07-19 09:48:10', '28', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+62895399085237', 'Fahrul', null, 'fahrul.java@gmail.com', '2018-07-17 23:06:08', '12', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+62895634598694', 'Fither Briano Seno Broto', null, 'senobroto01@gmail.com', '2018-07-19 13:57:03', '38', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6289680868343', 'Go Frendi Gunawan', null, 'gofrendiasgard@gmail.com', '2018-07-21 15:21:52', '56', '1996-11-25', '', '', '0');
INSERT INTO `tb_customer` VALUES ('+6289683397895', 'fandy ahnad', null, 'fandyjoansyah@gmail.com', '2018-07-17 21:46:58', '9', '1996-11-25', '', '', '0');

-- ----------------------------
-- Table structure for tb_detalkeahlian
-- ----------------------------
DROP TABLE IF EXISTS `tb_detalkeahlian`;
CREATE TABLE `tb_detalkeahlian` (
  `id_keahlian` int(11) NOT NULL,
  `telp_tukang` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_detalkeahlian
-- ----------------------------
INSERT INTO `tb_detalkeahlian` VALUES ('1', '1000000');
INSERT INTO `tb_detalkeahlian` VALUES ('2', '1000000');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '085791858101');
INSERT INTO `tb_detalkeahlian` VALUES ('2', '+6285706749886');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '+6285706749886');
INSERT INTO `tb_detalkeahlian` VALUES ('13', '+6281235133535');
INSERT INTO `tb_detalkeahlian` VALUES ('12', '+6281235133535');
INSERT INTO `tb_detalkeahlian` VALUES ('19', '+6281235133535');
INSERT INTO `tb_detalkeahlian` VALUES ('3', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('9', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('7', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('6', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('4', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('10', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('5', '+6281615177319');
INSERT INTO `tb_detalkeahlian` VALUES ('3', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('12', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('6', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('4', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('11', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('10', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('5', '+6282331452002');
INSERT INTO `tb_detalkeahlian` VALUES ('3', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('9', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('12', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('7', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('6', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('8', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('4', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('10', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('5', '+6285101302329');
INSERT INTO `tb_detalkeahlian` VALUES ('16', '+6283129751091');
INSERT INTO `tb_detalkeahlian` VALUES ('13', '+6283129751091');
INSERT INTO `tb_detalkeahlian` VALUES ('15', '+6283129751091');
INSERT INTO `tb_detalkeahlian` VALUES ('9', '+6283129751091');
INSERT INTO `tb_detalkeahlian` VALUES ('15', '+6289683397895');
INSERT INTO `tb_detalkeahlian` VALUES ('12', '+6289683397895');
INSERT INTO `tb_detalkeahlian` VALUES ('20', '+6289683397895');
INSERT INTO `tb_detalkeahlian` VALUES ('18', '+6289683397895');
INSERT INTO `tb_detalkeahlian` VALUES ('3', '+6283835720999');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '+6283835720999');
INSERT INTO `tb_detalkeahlian` VALUES ('3', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('9', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('7', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('6', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('1', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('8', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('4', '+6282232864345');
INSERT INTO `tb_detalkeahlian` VALUES ('5', '+6282232864345');

-- ----------------------------
-- Table structure for tb_keahlian
-- ----------------------------
DROP TABLE IF EXISTS `tb_keahlian`;
CREATE TABLE `tb_keahlian` (
  `id_keahlian` int(11) NOT NULL AUTO_INCREMENT,
  `nama_keahlian` varchar(100) NOT NULL,
  PRIMARY KEY (`id_keahlian`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_keahlian
-- ----------------------------
INSERT INTO `tb_keahlian` VALUES ('1', 'Pengecatan');
INSERT INTO `tb_keahlian` VALUES ('2', 'Instalasi Listrik');
INSERT INTO `tb_keahlian` VALUES ('3', 'Atap');
INSERT INTO `tb_keahlian` VALUES ('4', 'Plafon');
INSERT INTO `tb_keahlian` VALUES ('5', 'Tembok');
INSERT INTO `tb_keahlian` VALUES ('6', 'Lantai');
INSERT INTO `tb_keahlian` VALUES ('7', 'Kusen, Pintu & jendela');
INSERT INTO `tb_keahlian` VALUES ('8', 'Pengelasan');
INSERT INTO `tb_keahlian` VALUES ('9', 'Instalasi Air');
INSERT INTO `tb_keahlian` VALUES ('10', 'Taman');
INSERT INTO `tb_keahlian` VALUES ('11', 'Relief');
INSERT INTO `tb_keahlian` VALUES ('12', 'Kolam');
INSERT INTO `tb_keahlian` VALUES ('13', 'Furnitur');
INSERT INTO `tb_keahlian` VALUES ('14', 'Wallpaper');
INSERT INTO `tb_keahlian` VALUES ('15', 'Gorden');
INSERT INTO `tb_keahlian` VALUES ('16', 'AC');
INSERT INTO `tb_keahlian` VALUES ('17', 'Kulkas');
INSERT INTO `tb_keahlian` VALUES ('18', 'TV');
INSERT INTO `tb_keahlian` VALUES ('19', 'Mesin Cuci');
INSERT INTO `tb_keahlian` VALUES ('20', 'Kompor Gas');

-- ----------------------------
-- Table structure for tb_mst_promo
-- ----------------------------
DROP TABLE IF EXISTS `tb_mst_promo`;
CREATE TABLE `tb_mst_promo` (
  `kode_promo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `syarat` double NOT NULL,
  `persen` double NOT NULL,
  `cashback` int(11) NOT NULL,
  `lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_berakhir` datetime DEFAULT NULL,
  `url_foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `max_use` int(11) NOT NULL,
  `is_aktif` int(11) NOT NULL,
  PRIMARY KEY (`kode_promo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_mst_promo
-- ----------------------------
INSERT INTO `tb_mst_promo` VALUES ('BENERIN45', 'Promo Tujuh Belasan', '500', '5', '0', 'CFD Malang', '2018-08-03 20:59:03', '2018-08-03 20:59:59', '2018-09-01 20:59:11', 'dummy', 'tidak ada', '481', '1');
INSERT INTO `tb_mst_promo` VALUES ('CFDMALANG', 'CFD', '100', '2', '500', 'Malang', '2018-08-02 16:45:40', '2018-08-02 16:46:00', '2018-08-02 16:45:46', 'dummy', 'tidak ada', '1', '1');

-- ----------------------------
-- Table structure for tb_mst_voucher
-- ----------------------------
DROP TABLE IF EXISTS `tb_mst_voucher`;
CREATE TABLE `tb_mst_voucher` (
  `kode_voucher` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `tgl_dibuat` datetime DEFAULT NULL,
  `butuh_point` int(11) NOT NULL,
  `cashback` int(11) NOT NULL,
  `is_aktif` int(11) NOT NULL,
  PRIMARY KEY (`kode_voucher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_mst_voucher
-- ----------------------------

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token_customer` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `token_tukang` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_detail_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `altitude_alamat` double NOT NULL,
  `latitude_alamat` double NOT NULL,
  `telp_alternatif` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail_deskripsi_order` text COLLATE utf8_unicode_ci NOT NULL,
  `catatan_tambahan` text COLLATE utf8_unicode_ci,
  `tgl_pengerjaan` datetime DEFAULT NULL,
  `tgl_selesai` datetime DEFAULT NULL,
  `tgl_selesai_close` datetime DEFAULT NULL,
  `tgl_order_dibuat` datetime DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `harga_total` bigint(255) DEFAULT NULL,
  `harga_transfer` int(11) NOT NULL,
  `urutan_harga` int(11) NOT NULL,
  `telp_tukang` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp_customer` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `saran_pesan` text COLLATE utf8_unicode_ci,
  `is_taken` text COLLATE utf8_unicode_ci NOT NULL,
  `is_deal` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `FotoPembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(25) NOT NULL,
  `biaya_fixed` bigint(20) NOT NULL,
  `kategori` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `WaktuSelesaiGaransi` datetime NOT NULL,
  `batas_waktu_pembayaran` datetime NOT NULL,
  `sudah_bayar` int(4) NOT NULL,
  `sudah_upload` int(4) NOT NULL DEFAULT '0',
  `tgl_bayar` datetime DEFAULT NULL,
  `expiry` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`id_order`),
  KEY `telp_customer` (`telp_customer`),
  KEY `telp_tukang` (`telp_tukang`),
  CONSTRAINT `tb_order_ibfk_1` FOREIGN KEY (`telp_customer`) REFERENCES `tb_customer` (`telp_customer`),
  CONSTRAINT `tb_order_ibfk_2` FOREIGN KEY (`telp_tukang`) REFERENCES `tb_tukang` (`telp_tukang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES ('A42180731124923', 'dFxi-gOCqc8:APA91bGxq4tcMm5ZYnRMQtlS5HCjwQ1lmbCz_hKlgOsUnyMi9M5FzQYSdO4lRB4lLRpy8R2uDhoXnxBwjBfckVgvIFyTdWjmczxTFoMk5g5J61lsI5BS5IIR8y5Klkl29HALUK4iGSG63w37QYIAsYsKmdlGCCDzRg', '', 'http://benerin.xyz/api_benerin/customer/upload/img5873.jpg', 'Instalasi Listrik', 'Cupak Jatikerto Kromengan Malang *JL HM ISMAIL Rt14 RW02 Jatikerto, Kromengan, Kab Malang', '111.111', '-11.1111', null, 'rusak pada kabel terputus', 'konfirmasi dahulu', null, null, null, '2018-07-31 12:50:37', 'C', null, '0', '0', 'null', '+6285809060691', '', 'NO', '', '', '4', '0', '', '2018-09-01 14:50:44', '2018-09-01 14:50:44', '0', '0', null, 'NO');
INSERT INTO `tb_order` VALUES ('A63180731223147', 'dHoEpA3tuCA:APA91bHC0B9c3y7orELqRWh8QiiH4AalEiFJ7TrIYczANinGmJXM3hR6fLdehsPVoLA-mFMcJo09oE2NOi9wDGe9Du2EKKZIO9llj4EoeBjrO7Wir9KsUag_RR8OjkP__oH1zO39TAN6zQVPGqnljVLfA48nlnWmBw', 'eXdDlWuml1I:APA91bFmkGfc920KtVEW3aTQFmeVz1uoeuk845b_GJHnQrKsVRt_c_YjmXnpEVrWEJYRUqb4cTzPOMN54fnU5XAfWcTJ-VXkcgCK4MjuJ5F_xB_8vCmABNDmaaUr3iSwA3CW-JOwQZkWZUeFh0KMpAXV-oJvlVZl8g', 'http://benerin.xyz/api_benerin/customer/upload/img1794.jpg', 'Pengecatan', 'Jalan Anggrek Bulan Jatimulyo Kecamatan Lowokwaru Kota Malang *jalan anggrek bulan no. 12', '111.111', '-11.1111', null, 'mengecat tembok', 'area yg dicat belakang lemari, bisa dikerjakan besok?', '2018-08-01 12:00:00', '2018-08-02 12:00:00', '2018-08-02 11:20:25', '2018-07-31 22:32:21', 'O', '174500', '174501', '1', '+6285257814417', '+6285604044550', '', 'YES', 'YES', 'http://benerin.xyz/api_benerin/customer/uploadPembayaran/img9885.jpg', '5', '0', '', '2018-09-01 14:50:44', '2018-09-01 14:50:44', '0', '0', null, 'NO');
INSERT INTO `tb_order` VALUES ('N8180721195930', 'c7-14UrlhUo:APA91bHYZmfKYZvEzbIJN-lcUxqFRBFwpMPk1UABiYUtmmnBQFPxdWT2_aVu_ME3LGL3vSrEtsxo7Vg6OZfn5_Dx0j6ThhsvxdzaXj9eU55qzy_XpOGB1gC5VSbakPWdUOf1CEfOd6Ds2otmK8vuqzUN8jd5MN2eDQ', 'dHhP_B1AgtQ:APA91bEhWgiKc1qYG7X_QmqxBauOLcFd8xuwZsv1MOze4hw3kamhPl1yF0KlKphiUpV5wlLAOt2oCHXVvoL9CI2_IDa2fDYUDt0Pr36XNrutlYFyLA4pRk7r8PF1OC36odIGNi1jknkwi3-MqzxT_pV-g4XPhjcg_Q', 'http://benerin.xyz/api_benerin/customer/upload/img6955.jpg', 'Kusen & Pintu', 'Jalan Werkudoro Polehan Blimbing Kota Malang *test', '111.111', '-11.1111', null, 'test', 'test', '2018-07-21 20:03:00', '2018-07-28 17:03:00', '2018-07-21 20:43:59', '2018-07-21 19:59:32', 'F', '2000000', '2000001', '1', '+6282131238161', '+6285791104447', 'kerjaan nya apik', 'YES', 'NO', 'http://benerin.xyz/api_benerin/customer/uploadPembayaran/img6627.jpg', '5', '0', '', '2018-09-01 14:50:44', '2018-09-01 14:50:44', '1', '1', null, 'NO');
INSERT INTO `tb_order` VALUES ('N8180721201936', 'c7-14UrlhUo:APA91bHYZmfKYZvEzbIJN-lcUxqFRBFwpMPk1UABiYUtmmnBQFPxdWT2_aVu_ME3LGL3vSrEtsxo7Vg6OZfn5_Dx0j6ThhsvxdzaXj9eU55qzy_XpOGB1gC5VSbakPWdUOf1CEfOd6Ds2otmK8vuqzUN8jd5MN2eDQ', 'c0_34AE_5JY:APA91bFF6_r3_nmp-wd2BMCeZP4PYY1XLPQK4cunm9MT3gFVffJrznodLoMwpUDsvcrWFlAD_FvwgcOoq_8F-b_oJPeRvIU9WH_YwJ62OWpeibKP9VHa4x8hgMLygHrpIbPZgZqDhcuBZ3BbyI8ZB6Yl6OE1M6S3Mw', 'http://benerin.xyz/api_benerin/customer/upload/img4169.jpg', 'Pengecatan', 'Jalan Werkudoro Polehan Blimbing Kota Malang *no 15 ', '111.111', '-11.1111', null, 'cat test', 'ok', '2018-07-22 07:00:00', '2018-07-29 04:20:00', '2018-07-21 20:23:56', '2018-07-21 20:19:39', 'F', '1000000', '1000001', '1', '+6285232704933', '+6285791104447', 'ok', 'YES', 'YES', 'http://benerin.xyz/api_benerin/customer/uploadPembayaran/img8979.jpg', '5', '0', '', '2018-09-01 14:50:44', '2018-09-01 14:50:44', '1', '1', null, 'NO');
INSERT INTO `tb_order` VALUES ('S59180721164154', 'dOuzA2-1C7o:APA91bGBYfal9Tog3icputJl3madM36H80trMRkBK4LmMz3uB2X04wVH5dw_lKB7fMZVb-H1FLeYVZvo_uZBlZYEiqGbFL6IrVWuqEDoRuFipMptQdC6PS-MEjqPYV61utZ6AKE_GONO9WEwAk1Xbij01Ej1BUcj4g', 'ds7NYJopLg0:APA91bHWVQhp4c4hvdXrIAnhBynqefpCRz8SASDKOuHt5kKSid8mIPrgFUYOSRMCwLC0mx_ITq4RCDS7kxSJda2dR9C6u17okszA1JS6c7ZmtTT--vLJt1osH0e6BIHPyLEWXfpgeTeBViwOX_r4cdj7WKLSxo_ISQ', 'http://benerin.xyz/api_benerin/customer/upload/img3892.jpg', 'Pengecatan', 'Jalan Borobudur Gang 2 Blimbing Blimbing Kota Malang *jl Borobudur gang 4b no 10 RT 2 RW 8', '111.111', '-11.1111', null, 'cat dinding ruang tamu', 'konfirmasi dahulu sebelum melakukan survey', null, null, null, '2018-07-21 16:41:59', 'C', null, '0', '0', '+6282232864345', '+6282233816676', 'sip\n', 'YES', '', '', '3', '0', '', '2018-09-01 14:50:44', '2018-09-01 14:50:44', '0', '0', null, 'NO');
INSERT INTO `tb_order` VALUES ('Y71180731160554', 'exf-8xQcgRY:APA91bEG9whvG2S4OPjU_Ns0ZkXlXZhc8eAcuVooJ6OdJ8cjesMqifDcFgpwdrWX8wRLFG6nTjmAD1KWPyWKvs-5a00qVrhxhuo6zN_KtzD4QeWNklJ9FBWexcWkLvZXgDqI79wflShTVPeEgPXrlxfXnf5lWf0mXA', 'cq1P7mbiSyQ:APA91bHk_iI1fYeLip19MCdkUpxoZW4sErN_u5ZMw7mbu-iP31_Kbl1aJuumzs1ShyRImfV1R5dBmW2X1pRcwsk3axwf6S0k23w-TYNwHM8mfX2EZgTSk0n7HFR2yQ6KzYl2741XBh40IUmq3YliDU0jIKFAwbCORg', 'imgkosong', 'Instalasi Listrik', 'Jalan Villa Bukit Sengkaling Dusun Klandungan Landungsari Dau *villa bukit Sengkaling AP-1', '111.111', '-11.1111', null, 'Listrik di rumah sering (istilahnya) njeglek kl posisi mesin cuci (tipe bukaan depan) sdg menguras tabung. Tp dlm pemakaian mesin cuci sehari2, tdk ada masalah (listrik mengalir normal.', 'Dengan deskripsi tsb diatas, bisakah sy diinfokan estimasi anggaran perbaikanx? Dan mohon Konfirmasi terlebih dahulu sebelum datang. Tks', null, null, null, '2018-07-31 16:06:34', 'O', null, '0', '0', '+6285257814417', '+6281233448041', null, 'YES', '', '', '0', '0', '', '2018-09-01 14:50:44', '2018-09-01 14:50:44', '0', '0', null, 'NO');

-- ----------------------------
-- Table structure for tb_tr_dapat_point
-- ----------------------------
DROP TABLE IF EXISTS `tb_tr_dapat_point`;
CREATE TABLE `tb_tr_dapat_point` (
  `telp_customer` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `kode_dapat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banyak_point` int(11) NOT NULL,
  `sumber_dapat` int(11) NOT NULL,
  `tgl_dapat` datetime DEFAULT NULL,
  KEY `telp_customer` (`telp_customer`),
  CONSTRAINT `tb_tr_dapat_point_ibfk_1` FOREIGN KEY (`telp_customer`) REFERENCES `tb_customer` (`telp_customer`),
  CONSTRAINT `tb_tr_dapat_point_ibfk_2` FOREIGN KEY (`telp_customer`) REFERENCES `tb_customer` (`telp_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_tr_dapat_point
-- ----------------------------
INSERT INTO `tb_tr_dapat_point` VALUES ('+6285604044550', 'A63180731223147', '20', '1', '2018-08-04 02:06:24');
INSERT INTO `tb_tr_dapat_point` VALUES ('+6285604044550', 'A63180731223147', '20', '1', '2018-08-04 02:18:15');

-- ----------------------------
-- Table structure for tb_tr_promo
-- ----------------------------
DROP TABLE IF EXISTS `tb_tr_promo`;
CREATE TABLE `tb_tr_promo` (
  `telp_customer` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `id_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_promo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `terakhir_digunakan` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `use_ke` int(11) NOT NULL,
  KEY `telp_customer` (`telp_customer`),
  KEY `kode_promo` (`kode_promo`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `tb_tr_promo_ibfk_1` FOREIGN KEY (`telp_customer`) REFERENCES `tb_customer` (`telp_customer`),
  CONSTRAINT `tb_tr_promo_ibfk_2` FOREIGN KEY (`kode_promo`) REFERENCES `tb_mst_promo` (`kode_promo`),
  CONSTRAINT `tb_tr_promo_ibfk_3` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_tr_promo
-- ----------------------------
INSERT INTO `tb_tr_promo` VALUES ('+6285604044550', 'A63180731223147', 'BENERIN45', '2018-08-04 01:17:48', '1');
INSERT INTO `tb_tr_promo` VALUES ('+6285604044550', 'A63180731223147', 'BENERIN45', '2018-08-04 01:38:00', '1');
INSERT INTO `tb_tr_promo` VALUES ('+6285604044550', 'A63180731223147', 'BENERIN45', '2018-08-04 01:46:19', '1');
INSERT INTO `tb_tr_promo` VALUES ('+6285604044550', 'A63180731223147', 'BENERIN45', '2018-08-04 02:05:12', '1');
INSERT INTO `tb_tr_promo` VALUES ('+6285604044550', 'A63180731223147', 'BENERIN45', '2018-08-04 02:24:37', '1');

-- ----------------------------
-- Table structure for tb_tr_tukar_point
-- ----------------------------
DROP TABLE IF EXISTS `tb_tr_tukar_point`;
CREATE TABLE `tb_tr_tukar_point` (
  `telp_customer` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `id_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_tukar` int(11) NOT NULL,
  `banyak_point` int(11) NOT NULL,
  `kode_tukar` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_tukar` datetime DEFAULT NULL,
  KEY `telp_customer` (`telp_customer`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `tb_tr_tukar_point_ibfk_1` FOREIGN KEY (`telp_customer`) REFERENCES `tb_customer` (`telp_customer`),
  CONSTRAINT `tb_tr_tukar_point_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_tr_tukar_point
-- ----------------------------
INSERT INTO `tb_tr_tukar_point` VALUES ('+6285604044550', 'A63180731223147', '1', '20', 'BENERIN45', '2018-08-04 00:55:04');
INSERT INTO `tb_tr_tukar_point` VALUES ('+6285604044550', 'A63180731223147', '1', '20', '', '2018-08-04 01:02:31');
INSERT INTO `tb_tr_tukar_point` VALUES ('+6285604044550', 'A63180731223147', '1', '20', '', '2018-08-04 01:24:49');
INSERT INTO `tb_tr_tukar_point` VALUES ('+6285604044550', 'A63180731223147', '1', '20', '', '2018-08-04 01:39:58');
INSERT INTO `tb_tr_tukar_point` VALUES ('+6285604044550', 'A63180731223147', '1', '60', 'BENERIN45', '2018-08-04 02:24:37');

-- ----------------------------
-- Table structure for tb_tr_voucher
-- ----------------------------
DROP TABLE IF EXISTS `tb_tr_voucher`;
CREATE TABLE `tb_tr_voucher` (
  `telp_customer` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `id_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_voucher` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_tukar` datetime NOT NULL,
  `is_used` int(11) NOT NULL,
  `tgl_digunakan` datetime NOT NULL,
  `is_aktif` int(11) NOT NULL,
  KEY `telp_customer` (`telp_customer`),
  KEY `kode_voucher` (`kode_voucher`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `tb_tr_voucher_ibfk_1` FOREIGN KEY (`telp_customer`) REFERENCES `tb_customer` (`telp_customer`),
  CONSTRAINT `tb_tr_voucher_ibfk_2` FOREIGN KEY (`kode_voucher`) REFERENCES `tb_mst_voucher` (`kode_voucher`),
  CONSTRAINT `tb_tr_voucher_ibfk_3` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_tr_voucher
-- ----------------------------

-- ----------------------------
-- Table structure for tb_tukang
-- ----------------------------
DROP TABLE IF EXISTS `tb_tukang`;
CREATE TABLE `tb_tukang` (
  `telp_tukang` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `token_tukang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_tukang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_tukang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_daftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `foto_ktp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_ktp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto_tukang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `jumlah_Pengerjaan` int(255) NOT NULL,
  `jumlah_nilai` int(11) NOT NULL,
  PRIMARY KEY (`telp_tukang`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_tukang
-- ----------------------------
INSERT INTO `tb_tukang` VALUES ('+6281235133535', 'eXdDlWuml1I:APA91bFmkGfc920KtVEW3aTQFmeVz1uoeuk845b_GJHnQrKsVRt_c_YjmXnpEVrWEJYRUqb4cTzPOMN54fnU5XAfWcTJ-VXkcgCK4MjuJ5F_xB_8vCmABNDmaaUr3iSwA3CW-JOwQZkWZUeFh0KMpAXV-oJvlVZl8g', 'Test2', 'test', '2018-07-18 19:57:34', '', '', '', '5', '6', '30');
INSERT INTO `tb_tukang` VALUES ('+6281615177319', 'eHr9hKd_liQ:APA91bGdXRn2jOsKmho6YPB3z2eTYx7BkcPf86nxjxH4_p-VyRaAh2jTzaHhDZN18qqKDb4myyoofz5tKZDYBIj4gthYh5XpRg1mSOg2ymxUl9TGtZON81AQ87h9Ror4IcQE4lXcf-YjSZ53ZIZ0v3ayBkngsUov1w', 'Riki Dian Purwanto', 'Dusun Depok Gading Kembang Kec. Jabung, RT 027/RW 004', '2018-07-18 16:51:48', '', '350717806960002', '09cb7-rikki.jpeg', '5', '1', '5');
INSERT INTO `tb_tukang` VALUES ('+6282131238161', 'dHhP_B1AgtQ:APA91bEhWgiKc1qYG7X_QmqxBauOLcFd8xuwZsv1MOze4hw3kamhPl1yF0KlKphiUpV5wlLAOt2oCHXVvoL9CI2_IDa2fDYUDt0Pr36XNrutlYFyLA4pRk7r8PF1OC36odIGNi1jknkwi3-MqzxT_pV-g4XPhjcg_Q', 'suhartono', 'Sumber suko Gang Salak Lawang RR 01 RW 01', '2018-07-21 20:44:22', '', '3507250311720002', '', '5', '1', '5');
INSERT INTO `tb_tukang` VALUES ('+6282232864345', 'ds7NYJopLg0:APA91bHWVQhp4c4hvdXrIAnhBynqefpCRz8SASDKOuHt5kKSid8mIPrgFUYOSRMCwLC0mx_ITq4RCDS7kxSJda2dR9C6u17okszA1JS6c7ZmtTT--vLJt1osH0e6BIHPyLEWXfpgeTeBViwOX_r4cdj7WKLSxo_ISQ', 'Dhefit Rianto', 'Bugis Krajan Soptorenggo Rt 03/Rw4 Kec. pakis, Kab. Malang', '2018-07-21 16:48:35', '', '3507211607940002', 'e961d-dhefit_rianto.jpeg', '3', '1', '3');
INSERT INTO `tb_tukang` VALUES ('+6282331452002', '', 'Nurul Huda ', 'Dusun Kertowinangun, RT026/RT006, Tangkilsari, Tajinan', '2018-07-18 16:52:46', '', '3573031512890003', '27e4f-nurul_huda.jpeg', '0', '0', '0');
INSERT INTO `tb_tukang` VALUES ('+6283129751091', 'eI_27LftMNk:APA91bHTZI00ZK90ij5IIk776nn-JcS5K6P7bge2JIPc_TB5Mf4lcCrRyRli9pxONHrPlXStTEUHdx5Jr8IiJw9VoUSguu4K_6_SO09a4CYYYFiABNDNMNpIUCgm0pD7vdHieQrrV_ozGO-jm78mjEbhErUQai_CHg', 'MAING', 'Malang', '2018-07-18 16:30:56', '', '', 'ad0a5-maing-edit.png', '4.4545454545455', '11', '49');
INSERT INTO `tb_tukang` VALUES ('+6283835720999', '', 'Krisna Junaedi', 'Jalan Bugis Gg 9, Saptorenggo Rt 02 Rw 01 Pakis, Kab. Malang', '2018-07-18 16:57:34', '', '3507181301830002', '', '0', '0', '0');
INSERT INTO `tb_tukang` VALUES ('+6285101302329', '', 'Syamsul hadi', 'Jalan Bromo V No. 20 Rt. 04 Rw. 10 Sisir Kota Batu', '2018-07-18 16:53:13', '', '3579011601820004', 'f3cd7-syamsul-hadi.jpeg', '0', '0', '0');
INSERT INTO `tb_tukang` VALUES ('+6285232704933', 'c0_34AE_5JY:APA91bFF6_r3_nmp-wd2BMCeZP4PYY1XLPQK4cunm9MT3gFVffJrznodLoMwpUDsvcrWFlAD_FvwgcOoq_8F-b_oJPeRvIU9WH_YwJ62OWpeibKP9VHa4x8hgMLygHrpIbPZgZqDhcuBZ3BbyI8ZB6Yl6OE1M6S3Mw', 'Agung Bayu Suseno', 'Jl Sumber Suko Gang Salak', '2018-07-21 20:44:03', '', '3507252901900004', '', '5', '1', '5');
INSERT INTO `tb_tukang` VALUES ('+6285257814417', 'cq1P7mbiSyQ:APA91bHk_iI1fYeLip19MCdkUpxoZW4sErN_u5ZMw7mbu-iP31_Kbl1aJuumzs1ShyRImfV1R5dBmW2X1pRcwsk3axwf6S0k23w-TYNwHM8mfX2EZgTSk0n7HFR2yQ6KzYl2741XBh40IUmq3YliDU0jIKFAwbCORg', 'Muklis', 'Jl. Mayjen Panjaitan Gang 17 A no 93', '2018-08-02 14:50:44', '', '', '', '5', '1', '5');
INSERT INTO `tb_tukang` VALUES ('+6285335674511', '', 'FAHRUL', 'jl malang', '2018-07-17 14:33:36', '', '', '', '0', '0', '0');
INSERT INTO `tb_tukang` VALUES ('+6285706749886', 'fEo50pU29wI:APA91bHQaN3xnL73aypyqnAXx9_I1_6IgXh_hzWS23FWBTw8fS_ToOZ7kIyMsXtQEgmIOzQ4wG1hB9MLuhpUAjFkyJWc32m9bcUmlI_ejKneB0kJQjeLEbZ91Jm7RQLlk9weWrmFd5Aw2l_brafn9VgJLggpp_43xQ', 'DIRTA', 'tumpang', '2018-07-18 15:43:42', '', '123456789', '', '4.25', '8', '34');
INSERT INTO `tb_tukang` VALUES ('+6285730083702', 'dKan8gd24rE:APA91bFxR1_WOuJ0SpQ1GK01s-2bgzdbGeYAdAzrhPfHL2aRnzdgvJwDvSzSpuNXd07wio54cujGqsi6uCSm9y8kfTOW0GmgwVJwHTT4xZxqCxe-XMdJsZegeHkh1dlwewodmUqLv7zNg6m0BpxC5CQjfbuFSCMilQ', 'FEBRI ', 'malang', '2018-08-02 13:21:16', '43821-screenshot-from-2018-05-28-17-46-48.png', '', '1848e-ubuntu-mate-vespa.jpg', '1', '1', '1');
INSERT INTO `tb_tukang` VALUES ('+6285730702661', 'dEcI3FUGloQ:APA91bEJPUQ0BabIpy72uEKhu29RpzNqr8_S6FlmAL9YgSULBPRNdnu3kszzMhkkBcBfeBzfErycitLhW2uIbeCzU1rcQjPIXu8WofI-4Ea5Yvr3VM2_Awpcjmbzadz4xoXSEYK-ZfW6zjOZOEMLAIDr8PC7Hc3W9A', 'RHESAL', 'malang', '2018-08-01 14:12:32', '', '', '', '4.4', '5', '22');
INSERT INTO `tb_tukang` VALUES ('+6285791104447', 'ebFsdEBtpYc:APA91bFUU--5Y7hqGltYJ0YuC1LdJAM_TbB0CBsLDWZ0L6-Ii7ZKefOC-zqf3TptAH-HNliZEDSUVDOJFlBIVzOYgtzK3pPdjfi3006FwFN-ovxzVbnNkYGbaGbVaqfTJA1vponcOLu0To29qOBWNBj3OlXeyWf0rw', 'NIKKI', 'malang', '2018-07-18 20:13:57', '', '', '26f31-man.png', '1.6078431372549', '51', '82');
INSERT INTO `tb_tukang` VALUES ('+6289683397895', 'c8Llem01SfU:APA91bE8YJ1ozEp0q7pwPSMeMoszRv1cZRFIsqQmcLUCYkf_FOeF7WzMordEr5PUYauyFnpge-jlQsz6IV9UYGKsXtxEEKNJ06NJSsUqUm2ObDW991I71BqnuBErpGeh6W1q7pQFL6IMtCbyNXaHHexclvtzr_S0jg', 'FANDY', 'malang', '2018-07-24 17:07:44', '4b18b-win_20170813_00_11_11_pro.jpg', '123123123', '315ba-a42hdxw.jpg', '4.4', '15', '66');
INSERT INTO `tb_tukang` VALUES ('null', 'null', 'null', 'null', '2018-07-31 12:52:41', 'null', 'null', 'null', '1.75', '4', '7');
SET FOREIGN_KEY_CHECKS=1;
