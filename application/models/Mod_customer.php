<?php

class Mod_customer extends CI_Model
{
	
	function query($query){
		$rev = strrev($query);
		$sql = base64_decode(base64_decode($rev));
		$data= $this->db->query($sql)->result();
		return (object)array(
			'data'=>$data);
	}

	function delete($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function update($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	
}


?>