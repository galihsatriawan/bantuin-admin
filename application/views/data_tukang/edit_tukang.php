<!DOCTYPE html>
<html>
<head>
	<?php
	$this->load->view('elemen/head');
	?>
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
					<a class="navbar-brand" href="#"><span>Benerin</span>Admin</a>
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
							<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
						</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
								</a>
								<div class="message-body"><small class="pull-right">3 mins ago</small>
									<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
								</a>
								<div class="message-body"><small class="pull-right">1 hour ago</small>
									<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
					<ul class="dropdown-menu dropdown-alerts">
						<li><a href="#">
							<div><em class="fa fa-envelope"></em> 1 New Message
								<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
								</a></li>
								<li class="divider"></li>
								<li><a href="#">
									<div><em class="fa fa-user"></em> 5 New Followers
										<span class="pull-right text-muted small">4 mins ago</span></div>
									</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div><!-- /.container-fluid -->
			</nav>
			<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
				<div class="profile-sidebar">
					<div class="profile-userpic">
						<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
					</div>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">Username</div>
						<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="divider"></div>
				<form role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
				</form>
				<?php 
				$this->load->view('elemen/menu')
				?> 
			</div><!--/.sidebar-->

			<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
				<div class="row">
					<ol class="breadcrumb">
						<li><a href="#">
							<em class="fa fa-home"></em>
						</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</div><!--/.row-->

				
				<div class="col-md-12">
					<div class="panel ">
						<div class="panel-heading">
							Table Data Tukang	
						</div>
						<div class="panel-body">
							<div class="table">
								<table class="table table-striped">
									<form action="<?php echo site_url('Tukang/proses_edit');?>" method="POST">
									<?php  
									foreach ($data->data as $row) {
										# code...
										?>
										<tr>
											<td>Telp Tukang :</td>
											<td><input type="hidden" class="form-control" name="telp_tukang" value="<?php echo $row->telp_tukang ?>">
											<input type="text" class="form-control" name="" value="<?php echo $row->telp_tukang ?>" disabled>
											</td>
										</tr> 
										<tr>
											<td>Nama Tukang </td>
											<td><input type="text" name="nama_tukang" value="<?php echo $row->nama_tukang ?>" class="form-control"></td>
										</tr>
										<tr>
											<td>Alamat Tukang</td>
											<td><input type="text" name="alamat_tukang" value="<?php echo $row->alamat_tukang ?>" class="form-control"></td>
										</tr>
										<tr>
											<td>Tgl Daftar</td>
											<td><input type="text" name="tgl_daftar" value="<?php echo $row->tgl_daftar ?>" class="form-control" disabled> </td>
										</tr>
										<tr>
											<td>No Ktp</td>
											<td><input type="text" name="no_ktp" value="<?php echo $row->no_ktp ?>" class="form-control"></td>
										</tr>
										<tr>
											<td>Rating</td>
											<td><input type="text" name="rating" value="<?php echo $row->rating ?>" class="form-control"></td>
										</tr>
										<tr>
											<td>Jumlah Pengerjaan</td>
											<td><input type="text" name="jumlah_pengerjaan" value="<?php echo $row->jumlah_Pengerjaan ?>" class="form-control"></td>
										</tr>
										<tr>
										<td></td>
										<td>
										
										<input type="Submit" name="" value="Simpan" class="btn btn-success col-lg-2">
									
										</td>
										</tr>
										<?php } ?>
										</form>
									</table>

								</div>
							</div>
							<div class="panel-footer">
								<!--<div class="input-group">
									<input id="btn-input" class="form-control input-md" placeholder="Type your message here..." type="text"><span class="input-group-btn">
									<button class="btn btn-primary btn-md" id="btn-chat">Send</button>
								</span></div>
							-->
						</div>

					</div>


					<?php 
					$this->load->view('elemen/footer');
					?>

				</body>
				</html>