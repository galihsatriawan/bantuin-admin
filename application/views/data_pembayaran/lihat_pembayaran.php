<!DOCTYPE html>
<html>
<head>
	<?php
	$this->load->view('elemen/head');
	?>
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
					<a class="navbar-brand" href="#"><span>Benerin</span>Admin</a>
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
							<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
						</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
								</a>
								<div class="message-body"><small class="pull-right">3 mins ago</small>
									<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
								</a>
								<div class="message-body"><small class="pull-right">1 hour ago</small>
									<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
					<ul class="dropdown-menu dropdown-alerts">
						<li><a href="#">
							<div><em class="fa fa-envelope"></em> 1 New Message
								<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
								</a></li>
								<li class="divider"></li>
								<li><a href="#">
									<div><em class="fa fa-user"></em> 5 New Followers
										<span class="pull-right text-muted small">4 mins ago</span></div>
									</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div><!-- /.container-fluid -->
			</nav>
			<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
				<div class="profile-sidebar">
					<div class="profile-userpic">
						<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
					</div>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">Username</div>
						<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="divider"></div>
				<form role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
				</form>
				<?php 
				$this->load->view('elemen/menu')
				?> 
			</div><!--/.sidebar-->

			<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
				<div class="row">
					<ol class="breadcrumb">
						<li><a href="#">
							<em class="fa fa-home"></em>
						</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</div><!--/.row-->

				
				<div class="col-md-12">
					<div class="panel panel-default ">
						<div class="panel-heading">
							Table Data Tukang
							<div class="pull-right"><input type="text" id="myInput" name="search" onkeyup="myFilter()" placeholder="cari ID Order"></div>
						</div>

						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped" id="myTable">
									<tr>
										<th>#</th>
										<th>Id Order</th>
										<th>Kategori Order</th>
										<th>alamat Order</th>
										<th>Nama Tukang</th>
										<th>Nama Customer</th>
										<th>status Pembayaran</th>
										<th>Detail</th>
									</tr>
									<?php  
									$no = 1;
									foreach ($data->data as $row) {
										# code...
										?>
										<?php if ($row->sudah_bayar==1) {
											# code...
											?>
											<tr class="success">
												<?php
											}else{
												?>
												<tr class="danger">
													<?php
												} 
												?>
												
												<td><?php echo $no; ?></td>
												<td><?php echo $row->id_order ?></td>
												<td><?php echo $row->title_order ?></td>
												<td><?php echo $row->alamat_detail_order ?></td>
												<td><?php echo $row->nama_tukang ?></td>
												<td><?php echo $row->nama_customer ?></td>
												<td><?php 
													if ($row->sudah_bayar==0) {
												# code...
														echo "Belum bayar";
													}else{
														echo "sudah bayar";
													}
													?>

												</td>
												<td>
													<?php echo anchor(base_url('index.php/Pembayaran/detail_pembayaran/'.urlencode($row->id_order )),'<button type="button" class="btn btn-primary glyphicon glyphicon-pencil">Detail</button>'); ?> 
												</td>
											</tr>
										</div>

										<?php 
										$no++;
									} ?>
								</table>

								<script>
								function myFilter() {
										var input, filter, table, tr, td, i;
										input = document.getElementById("myInput");
										filter = input.value.toUpperCase();
										table = document.getElementById("myTable");
										tr = table.getElementsByTagName("tr");
										for (i = 0; i < tr.length; i++) {
											td = tr[i].getElementsByTagName("td")[1];
											if (td) {
												if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
													tr[i].style.display = "";
												} else {
													tr[i].style.display = "none";
												}
											}       
										}
									}
								</script>
							</div>
						</div>
						<div class="panel-footer">
								<!--<div class="input-group">
									<input id="btn-input" class="form-control input-md" placeholder="Type your message here..." type="text"><span class="input-group-btn">
									<button class="btn btn-primary btn-md" id="btn-chat">Send</button>
								</span></div>
							-->
						</div>

					</div>


					<?php 
					$this->load->view('elemen/footer');
					?>

				</body>
				</html>