<!DOCTYPE html>
<html>
<head>
	<?php
	$this->load->view('elemen/head');
	?>
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
					<a class="navbar-brand" href="#"><span>Benerin</span>Admin</a>
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
							<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
						</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
								</a>
								<div class="message-body"><small class="pull-right">3 mins ago</small>
									<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
								</a>
								<div class="message-body"><small class="pull-right">1 hour ago</small>
									<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
					<ul class="dropdown-menu dropdown-alerts">
						<li><a href="#">
							<div><em class="fa fa-envelope"></em> 1 New Message
								<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
								</a></li>
								<li class="divider"></li>
								<li><a href="#">
									<div><em class="fa fa-user"></em> 5 New Followers
										<span class="pull-right text-muted small">4 mins ago</span></div>
									</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div><!-- /.container-fluid -->
			</nav>
			<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
				<div class="profile-sidebar">
					<div class="profile-userpic">
						<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
					</div>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">Username</div>
						<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="divider"></div>
				<form role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
				</form>
				<?php 
				$this->load->view('elemen/menu')
				?> 
			</div><!--/.sidebar-->

			<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
				<div class="row">
					<ol class="breadcrumb">
						<li><a href="#">
							<em class="fa fa-home"></em>
						</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</div><!--/.row-->

				
				<div class="col-md-12">
					<div class="panel panel-default ">
						<div class="panel-heading">
							Table Data Tukang	
						</div>
						<div class="panel-body">
							<div class="table">
								<table class="table table-striped">
									<form action="<?php echo site_url('Tukang/proses_edit');?>" method="POST">
										<?php  
										foreach ($data->data as $row) {
										# code...
											?>
											<tr>
												<td>id order</td>
												<td><input type="hidden" class="form-control" name="id_order" value="<?php echo $row->id_order ?>">
													<input type="text" class="form-control" name="" value="<?php echo $row->id_order ?>" disabled>
												</td>
											</tr> 
											<tr>
												<td>title Order</td>
												<td><input type="text" name="title_order" value="<?php echo $row->title_order ?>" class="form-control" disabled></td>
											</tr>
											<tr>
												<td>Alamat order</td>
												<td><input type="text" name="alamat_detail_order" value="<?php echo $row->alamat_detail_order ?>" class="form-control" disabled></td>
											</tr>
											<tr>
												<td>Nama/No Hp User</td>
												<td><input type="text" name="detail_deskripsi_order" value="<?php echo $row->telp_customer ?>/<?php echo $row->nama_tukang ?>" class="form-control" disabled> </td>
											</tr>
											<tr>
												<td>Nama/Notelpon Tukang</td>
												<td><input type="text" name="detail_deskripsi_order" value="<?php echo $row->telp_tukang ?>/<?php echo $row->nama_tukang ?>" class="form-control" disabled> </td>
											</tr>
											<tr>
												<td>Foto Kerusakan</td>
												<td><a href="<?php echo $row->image_order ?>"><img src="<?php echo $row->image_order ?>" class="img-thumbnail" alt="Belum Tersedia" width="150" height="150"></a>
												</td>
											</tr>
											<tr>
												<td>Foto Pembayaran</td>
												<td><a href="<?php echo $row->FotoPembayaran ?>"><img src="<?php echo $row->FotoPembayaran ?>" class="img-thumbnail" alt="Belum Tersedia" width="150" height="150"></a>
												</td>
											</tr>
											<!--
											<tr>
												<td>Catatan Tambahan</td>
												<td><input type="text" name="catatan_tambahan" value="<?php echo $row->catatan_tambahan ?>" class="form-control"></td>
											</tr>
											<tr>
												<td>tgl pengerjaan</td>
												<td><input type="text" name="tgl_pengerjaan" value="<?php echo $row->tgl_pengerjaan ?>" class="form-control"></td>
											</tr>
											<tr>
												<td>tgl Selesai</td>
												<td><input type="text" name="tgl_selesai" value="<?php echo $row->tgl_selesai ?>" class="form-control"></td>
											</tr>
											<tr>
												<td></td>
												<td>


												</td>
											</tr>
										-->
									</form>
								</table>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel panel-default chat">
						<div class="panel-heading">
							Detail	
						</div>
						<div class="panel-body">
							<table class="table table-striped">
								<tr>
									<th>detail deskripsi</th>
									<th>catatan Tambah</th>
									<th>status pengambilan</th>
									<th>Status deal</th>
									<th>status order</th>
									<th>tgl order dibuat</th>
									<th>tgl_pengerjaan</th>
									<th>tgl selesai</th>
									<th>batas Waktu Pembayaran</th>
									<th>rating</th>
									<th>total biaya</th>
									<th>status Pembayaran</th>
									<th>Konfirmasi</th>
								</tr>
								<tr>
									<td><?php echo $row->detail_deskripsi_order ?></td>
									<td><?php echo $row->catatan_tambahan ?></td>
									<td><?php if($row->is_taken=="YES"){
										echo "Sudah diambil tukang";
									}else{
										echo "Belum di ambil tukang";
									} ?></td>
									<td><?php echo $row->is_deal ?></td>
									<td><?php 
									if ($row->status = "K") {
										echo "Konfirmasi";
									}else if($row->status = "C"){
										echo "Cancel";
									}else if($row->status = "O"){
										echo "On going";
									}else if($row->status = "F"){
										echo "Finish";
									}
									?></td>
									<td><?php echo $row->tgl_order_dibuat ?></td>
									<td><?php echo $row->tgl_pengerjaan ?></td>
									<td><?php echo $row->tgl_selesai ?></td>
									<td><?php echo $row->batas_waktu_pembayaran?></td>
									<td><?php echo $row->rating ?></td>
									<td><?php echo $row->harga_total ?></td>

									<td><?php if($row->sudah_bayar==1){
										echo "lunas";
									}else{
										echo "belum lunas";
									} ?></td>
									<td>
										<?php 

// 										$awal  = date_create('1988-08-10');
// 										$akhir = date_create(); // waktu sekarang
// 										$diff  = date_diff( $awal, $akhir );

										// echo 'Selisih waktu: '
										// echo $diff->y . ' tahun, ';
										// echo $diff->m . ' bulan, ';
// echo $diff->d . ' hari, ';
// echo $diff->h . ' jam, ';
// echo $diff->i . ' menit, ';
// echo $diff->s . ' detik, ';
// // Output: Selisih waktu: 28 tahun, 5 bulan, 9 hari, 13 jam, 7 menit, 7 detik

// echo 'Total selisih hari : ' . $diff->days;

                                        
										$awalan=$row->batas_waktu_pembayaran;
										$awal  = date_create();
										$akhir = date_create($awalan); 
										$diff  = date_diff( $awal, $akhir );
                    
                                        $valueBatasWaktuAkhir = strtotime($awalan);
                                        $tanggalWaktuSekarang = date("Y-m-d H:i:s");
                                    	$valueWaktuSekarang = strtotime($tanggalWaktuSekarang);
					
										if ($row->FotoPembayaran != NULL) {
												# code...

											if ($row->sudah_bayar==1) {
												echo "Lunas";
											}else{
												if ($diff->d <=0) {
													if ($diff->i <=0) {
														# code...

														echo "sisa hari : ";
														echo $diff->d;
														echo "sisa menit : ";
														echo $diff->i;
														echo "Sudah Expired foto pembayaran ada";
														// echo "awal:";
														// echo $awal->format("Y/m/d h:i:s");
														// echo "akhir";
														// echo $akhir->format("Y/m/d h:i:s");
														// echo $diff->format('waktu tersisa :%m menit');
														// echo $diff->format('waktu tersisa :%d hari');
														echo anchor(base_url('index.php/Pembayaran/konfirmasi/'.urlencode($row->id_order)),'<button type="button" class="btn btn-danger glyphicon glyphicon-pencil">Confirm</button>');	


													}else{
														echo "sisa hari : ";
														echo $diff->d;
														echo "sisa menit : ";
														echo $diff->i;
														echo anchor(base_url('index.php/Pembayaran/konfirmasi/'.urlencode($row->id_order)),'<button type="button" class="btn btn-danger glyphicon glyphicon-pencil">Confirm</button>');	
													}

												}else{

													echo "sisa hari : ";
													echo $diff->d;
													echo "sisa menit : ";
													echo $diff->i;

													echo anchor(base_url('index.php/Pembayaran/konfirmasi/'.urlencode($row->id_order)),'<button type="button" class="btn btn-danger glyphicon glyphicon-pencil">Confirm</button>');	
												}
											}

										}else{
										    //echo  $valueWaktuSekarang . "+".$valueBatasWaktuAkhir.$awalan;
										    //var_dump($valueWaktuSekarang);
										    //var_dump($valueWaktuSekarang > $valueBatasWaktuAkhir);
                                          if( ($valueWaktuSekarang > $valueBatasWaktuAkhir)){
                                              echo  "Expired";
                                          }
                                          /* Perbaikan Awal 
								// 			if ($diff->d <=0) {
								// 				if ($diff->i <=0) {
								// 						# code...

								// 					echo "sisa hari : ";
								// 					echo $diff->d;
								// 					echo "sisa menit : ";
								// 					echo $diff->i;
								// 					echo "Sudah Expired foto pembayaran tidak ada";
								// 					// echo "awal:";
								// 					// echo $awal->format("Y/m/d h:i:s");
								// 					// echo "akhir";
								// 					// echo $akhir->format("Y/m/d h:i:s");
								// 					// echo $diff->format('waktu tersisa :%m menit');
								// 					// echo $diff->format('waktu tersisa :%d hari');
								// 					echo anchor(base_url('index.php/Pembayaran/konfirmasi/'.urlencode($row->id_order)),'<button type="button" class="btn btn-danger glyphicon glyphicon-pencil">Confirm</button>');	


								// 				}
								
												else{

													echo "sisa hari : ";
													echo $diff->d;
													echo "sisa menit : ";
													echo $diff->i;
													echo anchor(base_url('index.php/Pembayaran/konfirmasi/'.urlencode($row->id_order)),'<button type="button" class="btn btn-danger glyphicon glyphicon-pencil">Confirm</button>');	
												}

											}
											*/
											else{

												echo "sisa hari : ";
												echo $diff->d;
												echo "sisa menit : ";
												echo $diff->i;
												echo anchor(base_url('index.php/Pembayaran/konfirmasi/'.urlencode($row->id_order)),'<button type="button" class="btn btn-danger glyphicon glyphicon-pencil">Confirm</button>');	

											}

												// echo "sudah terisi menunggu konfirmasi    ";
												// //$date_now = new DateTime($row->batas_waktu_pembayaran);
												// $date_end = date("Y/m/d h:i:s");
												// //$different = $date_now->date_diff($date_end);
												// echo $date_end;


										}


										?>


									</td>
								</tr>
							</table>
						</div>
					</div>

					<?php } ?>
					<div class="panel-footer">
								<!--<div class="input-group">
									<input id="btn-input" class="form-control input-md" placeholder="Type your message here..." type="text"><span class="input-group-btn">
									<button class="btn btn-primary btn-md" id="btn-chat">Send</button>
								</span></div>
							-->
						</div>

					</div>



					<?php 
					$this->load->view('elemen/footer');
					?>

				</body>
				</html>