		<ul class="nav menu">
			<li><a href="<?php echo base_url('index.php/Dashboard') ?>"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<!-- <li><a href="widgets.html"><em class="fa fa-calendar">&nbsp;</em> Widgets</a></li>
			<li><a href="charts.html"><em class="fa fa-bar-chart">&nbsp;</em> Grafik</a></li>
			<li><a href="elements.html"><em class="fa fa-toggle-off">&nbsp;</em> UI Elements</a></li>
			<li><a href="panels.html"><em class="fa fa-clone">&nbsp;</em> Alerts &amp; Panels</a></li> -->
			
			<li><a href="<?php echo base_url('index.php/Pembayaran') ?>"><em class="fa fa-dollar">&nbsp;</em> Pembayaran</a></li>
			<li><a href="<?php echo base_url('index.php/Grafik') ?>"><em class="fa fa-bar-chart">&nbsp;</em> Grafik</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> Master <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="<?php echo base_url('index.php/Tukang');
					 ?>">
						<span class="fa fa-arrow-right">&nbsp;</span> Data Tukang
					</a></li>
					<li><a class="" href="<?php echo base_url('index.php/Customer');
					 ?>">
						<span class="fa fa-arrow-right">&nbsp;</span> Data Customer
					</a></li>
					<li><a class="" href="<?php echo base_url('index.php/Keahlian');
					 ?>">
						<span class="fa fa-arrow-right">&nbsp;</span> Data Keahlian
					</a></li>
				</ul>
			</li>
			<li><a href="<?php echo base_url('index.php/Login/logout') ?>"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>