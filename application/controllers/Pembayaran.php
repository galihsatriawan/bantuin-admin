<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->helper('form','url');
		$this->load->model('Mod_pembayaran');

	}
	function kirim_notif($token,$pesan){
        // echo "Token Tukang :".$tukang."<br>";
// 			echo "Token Customer :".$customer;
		$title= "Pembayaran";
		$body = $pesan;
		$dest_token = array($token);
		define('API_ACCESS_KEY','AAAAEYIkQC8:APA91bGUVfqYFtDEx08P1CWLeurQBXEV-NPjLaJj_iScE7awad7wVb1fWy_-yZB3J96wBLwHpHquHYydiHxaJhqWQM1uJW-jG3c_d51OtCzVZYWTLSfdN_ZToKnIS2cp4JDsMnFZ5UAm');
		$url = 'https://fcm.googleapis.com/fcm/send';

		$registrationIds =$dest_token;
		// prepare the message
		$message = array( 
			'title'     => $title,
			'body'      => $body,
			'vibrate'   => 1,
			'sound'      => 1
			);
		$fields = array( 
			'registration_ids' => $registrationIds, 
		  // 'data'             => $message  --> Wrong written
			'notification'             => $message
			);
		$headers = array( 
			'Authorization: key='.API_ACCESS_KEY, 
			'Content-Type: application/json'
			);
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL,$url);
		curl_setopt( $ch,CURLOPT_POST,true);
		curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	public function konfirmasi($id){
		$id = urldecode($id);

		$sql = "
		select \n"
		. "o.id_order,\n"
		. "o.token_customer,\n"
		. "o.token_tukang,\n"
		. "o.sudah_bayar\n"
		. "from tb_customer as c, tb_order as o, tb_tukang as t WHERE t.telp_tukang=o.telp_tukang and c.telp_customer = o.telp_customer and  id_order ='".$id."'";

		$query = base64_encode(base64_encode($sql));
		$query = strrev($query);
		$data= $this->Mod_pembayaran->query($query);
		foreach ($data->data as $key) {
			$tukang= $key->token_tukang;
			$customer = $key->token_customer;
// 			echo $tukang;
// 			echo "<br>$customer";
			$pesan_customer = "Pembayaran Berhasil | Tukang segera melakukan pengerjaan";
			$pesan_tukang = "Segera Melakukan Proses Pengerjaan";

		// 	/* Untuk Customer */
             $title= "Pembayaran";
			$body = $pesan_customer;
			$dest_token = array($customer);
			define('API_ACCESS_KEY','AAAAEYIkQC8:APA91bGUVfqYFtDEx08P1CWLeurQBXEV-NPjLaJj_iScE7awad7wVb1fWy_-yZB3J96wBLwHpHquHYydiHxaJhqWQM1uJW-jG3c_d51OtCzVZYWTLSfdN_ZToKnIS2cp4JDsMnFZ5UAm');
			$url = 'https://fcm.googleapis.com/fcm/send';

			$registrationIds =$dest_token;
		// prepare the message
			$message = array( 
				'title'     => $title,
				'body'      => $body
				);
			$fields = array( 
				'registration_ids' => $registrationIds, 
		  // 'data'             => $message  --> Wrong written
				'data'             => $message
				);
			$headers = array( 
				'Authorization: key='.API_ACCESS_KEY, 
				'Content-Type: application/json'
				);
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL,$url);
			curl_setopt( $ch,CURLOPT_POST,true);
			curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
// 			echo $result;
			
			
		// 	/*  Untuk Tukang */
			$title= "Pembayaran";
			$body = $pesan_tukang;
			$dest_token = array($tukang);
		//	define('API_ACCESS_KEY','AAAAEYIkQC8:APA91bGUVfqYFtDEx08P1CWLeurQBXEV-NPjLaJj_iScE7awad7wVb1fWy_-yZB3J96wBLwHpHquHYydiHxaJhqWQM1uJW-jG3c_d51OtCzVZYWTLSfdN_ZToKnIS2cp4JDsMnFZ5UAm');
			$url = 'https://fcm.googleapis.com/fcm/send';

			$registrationIds =$dest_token;
		// prepare the message
			$message = array( 
				'title'     => $title,
				'body'      => $body
				);
			$fields = array( 
				'registration_ids' => $registrationIds, 
		  // 'data'             => $message  --> Wrong written
				'data'             => $message
				);
			$headers = array( 
				'Authorization: key='.API_ACCESS_KEY, 
				'Content-Type: application/json'
				);
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL,$url);
			curl_setopt( $ch,CURLOPT_POST,true);
			curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
			//echo $result;
            //echo kirim_notif($tukang,$pesan_tukang);

			$where = array('id_order'=>$id);
			$data = array('sudah_bayar' => "1");
			$this->Mod_pembayaran->update($where,$data,'tb_order');
			redirect(base_url('index.php/Pembayaran'));
		}
	}




	public function index(){

		$sql = "select \n"

    . "		o.id_order,\n"

    . "        o.title_order,\n"

    . "        o.alamat_detail_order,\n"

    . "        o.image_order,\n"

    . "        o.detail_deskripsi_order,\n"

    . "        o.catatan_tambahan,\n"

    . "        o.tgl_pengerjaan,\n"

    . "        o.tgl_selesai,\n"

    . "        o.tgl_order_dibuat,\n"

    . "        o.token_customer,\n"

    . "        o.token_tukang,\n"

    . "        o.status,\n"

    . "        o.harga_total,\n"

    . "		o.telp_tukang,\n"

    . "		o.telp_customer,\n"

    . "		o.saran_pesan,\n"

    . "		o.is_taken,\n"

    . "		o.is_deal,\n"

    . "		o.FotoPembayaran,\n"

    . "		o.rating,\n"

    . "		o.biaya_fixed,\n"

    . "		o.batas_waktu_pembayaran,\n"

    . "		o.sudah_bayar,\n"

    . "		o.sudah_upload,\n"

    . "		o.tgl_bayar,\n"

    . "		o.expiry,\n"

    . "		c.nama_customer,\n"

    . "		c.email,\n"

    . "		c.id_customer,\n"

    . "		t.nama_tukang,\n"

    . "		t.alamat_tukang,\n"

    . "		t.tgl_daftar,\n"

    . "		t.no_ktp,\n"

    . "		t.foto_tukang,\n"

    . "		t.rating\n"

    . "		from tb_customer as c, tb_order as o, tb_tukang as t WHERE t.telp_tukang=o.telp_tukang and c.telp_customer = o.telp_customer ORDER BY o.tgl_order_dibuat DESC";

		$query = base64_encode(base64_encode($sql));
		$query = strrev($query);
		$pack = array(
			'data' => $this->Mod_pembayaran->query($query),
			);
		$this->load->view('data_pembayaran/lihat_pembayaran',$pack);

	}

	public function tampil_tukang(){

		$query = base64_encode(base64_encode("select * from tb_tukang"));
		$query = strrev($query);
		$pack = array(
			'data' => $this->Mod_pembayaran->query($query),
			);
		//var_dump($pack);
		$this->load->view('data_tukang/lihat_tukang',$pack);
	}

	public function detail_pembayaran($id){
		$sql = "select \n"

		. "o.id_order,\n"

		. "o.title_order,\n"

		. "o.alamat_detail_order,\n"

		. "o.image_order,\n"

		. "o.detail_deskripsi_order,\n"

		. "o.catatan_tambahan,\n"

		. "o.tgl_pengerjaan,\n"

		. "o.tgl_selesai,\n"

		. "o.tgl_order_dibuat,\n"

		. "o.token_customer,\n"

		. "o.token_tukang,\n"

		. "o.status,\n"

		. "o.harga_total,\n"

		. "o.telp_tukang,\n"

		. "o.telp_customer,\n"

		. "o.saran_pesan,\n"

		. "o.is_taken,\n"

		. "o.is_deal,\n"

		. "o.FotoPembayaran,\n"

		. "o.rating,\n"

		. "o.biaya_fixed,\n"

		. "o.batas_waktu_pembayaran,\n"

		. "o.sudah_bayar,\n"

		. "o.sudah_upload,\n"

		. "o.tgl_bayar,\n"

		. "o.expiry,\n"

		. "c.nama_customer,\n"

		. "c.email,\n"

		. "c.id_customer,\n"

		. "t.nama_tukang,\n"

		. "t.alamat_tukang,\n"

		. "t.tgl_daftar,\n"

		. "t.no_ktp,\n"

		. "t.foto_tukang,\n"

		. "t.rating\n"

		. "from tb_customer as c, tb_order as o, tb_tukang as t WHERE t.telp_tukang=o.telp_tukang and c.telp_customer = o.telp_customer
		and  id_order='".urldecode($id)."' ";
		$query = base64_encode(base64_encode($sql));
		$query = strrev($query);
		$pack = array(
			'data' => $this->Mod_pembayaran->query($query),
			);
		
		$this->load->view('data_pembayaran/detail_pembayaran',$pack);
	}
	public function proses_edit(){
		$telp_tukang = $this->input->post('telp_tukang');
		$nama_tukang = $this->input->post('nama_tukang');
		$alamat = $this->input->post('alamat_tukang');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$no_ktp = $this->input->post('no_ktp');
		$data = array(
			'nama_tukang' => $nama_tukang,
			'alamat_tukang' => $alamat,
			'tgl_daftar' => $tgl_daftar,
			'no_ktp' => $no_ktp
			);
		$where = array('telp_tukang' => $telp_tukang);
		$this->Mod_pembayaran->update($where,$data,'tb_tukang');
		redirect('Tukang/tampil_tukang');

	}
	public function tambah_tukang(){

		$this->load->view('data_tukang/tambah_tukang');
	}

	public function proses_tambah_tukang(){
		$telp_tukang = $this->input->post('telp_tukang');
		$nama_tukang = $this->input->post('nama_tukang');
		$alamat = $this->input->post('alamat_tukang');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$no_ktp = $this->input->post('no_ktp');
		$data = array(
			'telp_tukang' => $telp_tukang,
			'nama_tukang' => $nama_tukang,
			'alamat_tukang' => $alamat,
			'tgl_daftar' => $tgl_daftar,
			'no_ktp' => $no_ktp,
			'tgl_daftar' =>  date("y-m-d h:i:s")
			);
		$this->Mod_pembayaran->input_data($data,'tb_tukang');
		redirect('Tukang/tampil_tukang');
	}

	public function delete_tukang($id){
		$where = array(
			'telp_tukang' => urldecode($id)
			);
		$this->Mod_pembayaran->delete($where,'tb_tukang');
		redirect('Tukang/tampil_tukang');
	}


}


?>