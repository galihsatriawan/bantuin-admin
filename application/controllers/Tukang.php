<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tukang extends CI_Controller{
	
/*	function __construct(){
		parent::__construct();
		$this->load->helper('form','url');
		$this->load->model('Mod_tukang');

	}

	public function tampil_tukang(){

		$query = base64_encode(base64_encode("select * from tb_tukang"));
		$query = strrev($query);
		$pack = array(
			'data' => $this->Mod_tukang->query($query),
			);
		//var_dump($pack);
		$this->load->view('data_tukang/lihat_tukang',$pack);
	}

	public function edit_tukang($id){
		$query = base64_encode(base64_encode("select * from tb_tukang where telp_tukang='".urldecode($id)."' "));
		$query = strrev($query);
		$pack = array(
			'data' => $this->Mod_tukang->query($query),
			);
		$this->load->view('data_tukang/edit_tukang',$pack);
	}
	public function proses_edit(){
		$telp_tukang = $this->input->post('telp_tukang');
		$nama_tukang = $this->input->post('nama_tukang');
		$alamat = $this->input->post('alamat_tukang');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$no_ktp = $this->input->post('no_ktp');
		$data = array(
			'nama_tukang' => $nama_tukang,
			'alamat_tukang' => $alamat,
			'tgl_daftar' => $tgl_daftar,
			'no_ktp' => $no_ktp
			);
		$where = array('telp_tukang' => $telp_tukang);
		$this->Mod_tukang->update($where,$data,'tb_tukang');
		redirect('Tukang/tampil_tukang');
		
	}
	public function tambah_tukang(){
	
		$this->load->view('data_tukang/tambah_tukang');
	}

	public function proses_tambah_tukang(){

		/*$telp_tukang = $this->input->post('telp_tukang');
		$nama_tukang = $this->input->post('nama_tukang');
		$alamat = $this->input->post('alamat_tukang');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$no_ktp = $this->input->post('no_ktp');
		$data = array(
			'telp_tukang' => $telp_tukang,
			'nama_tukang' => $nama_tukang,
			'alamat_tukang' => $alamat,
			'tgl_daftar' => $tgl_daftar,
			'no_ktp' => $no_ktp,
			'tgl_daftar' =>  date("y-m-d h:i:s")
			);
		$this->Mod_tukang->input_data($data,'tb_tukang');
		redirect('Tukang/tampil_tukang');
		$config['upload_path']          = './assets/foto_tukang/';
	$config['allowed_types']        = 'gif|jpg|png';
	$config['max_size']             = 100;
	$config['max_width']            = 1024;
	$config['max_height']           = 768;

	$this->load->library('upload', $config);

	if ( ! $this->upload->do_upload('foto_ktp')){
		$error = array('error' => $this->upload->display_errors());
		$this->load->view('data_tukang/tambah_tukang', $error);
	}else{
		$data = array('upload_data' => $this->upload->data());
		$this->load->view('data_tukang/lihat_tukang', $data);
	}
  }

	public function delete_tukang($id){
		$where = array(
			'telp_tukang' => urldecode($id)
			);
		$this->Mod_tukang->delete($where,'tb_tukang');
		redirect('Tukang/tampil_tukang');
	}*/
	function __construct() {
        parent::__construct();
    }

	public function index() {
        // instance object
        $crud = new grocery_CRUD();
        // pilih tabel yang akan digunakan
        $crud->set_table('tb_tukang');
        // simpan hasilnya kedalam variabel output
        $crud->set_relation_n_n('keahlian', 'tb_detalkeahlian', 'tb_keahlian', 'telp_tukang', 'id_keahlian', 'nama_keahlian');
        $crud->columns('telp_tukang','nama_tukang','alamat_tukang','foto_ktp','keahlian');
        $crud->field_type('keahlian','dropdown');
        $crud->add_fields(array('telp_tukang','no_ktp','nama_tukang','alamat_tukang','foto_ktp','foto_tukang'));
        $crud->edit_fields(array('telp_tukang','no_ktp','nama_tukang','alamat_tukang','foto_ktp','foto_tukang','keahlian'));
        $crud->set_field_upload('foto_ktp','assets/gambar/foto_ktp/');
        $crud->set_field_upload('foto_tukang','assets/gambar/foto_tukang/');
        $output = $crud->render();
        // tampilkan di view 
        //$this->_example_output($output);
        $this->load->view('data_tukang/lihat_tukang.php', $output);
    }
}


?>