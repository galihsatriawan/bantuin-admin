<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller{
	
/*	function __construct(){
		parent::__construct();
		$this->load->helper('form','url');
		$this->load->model('Mod_customer');
	}
	public function tampil_customer(){
		$query = base64_encode(base64_encode("select * from tb_customer"));
		$query = strrev($query);
		$pack = array(
			'data' => $this->Mod_customer->query($query)
			);
		//var_dump($pack);
		$this->load->view('data_customer/lihat_customer',$pack);
	}*/
	function __construct() {
        parent::__construct();
    }

	public function index() {
        // instance object
        $crud = new grocery_CRUD();
        // pilih tabel yang akan digunakan
        $crud->set_table('tb_customer');
        // simpan hasilnya kedalam variabel output
        $crud->columns('telp_customer','nama_customer','alamat_customer','email','foto_customer');
        $crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
        $output = $crud->render();
        // tampilkan di view 
        //$this->_example_output($output);
        $this->load->view('data_customer/lihat_customer.php', $output);
    }
}
?>